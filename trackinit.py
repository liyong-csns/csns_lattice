#!/usr/bin/env pyORBIT

import os
import shutil
import argparse
from subprocess import Popen, PIPE

def create_newtrack(job_type, name):
    process = Popen(['which', 'tracker.py'], stdout=PIPE, stderr=PIPE)
    stdout, stderr = process.communicate()
    if job_type == 0:
        job_name = 'config_empirical.py'
    elif job_type == 1:
        job_name = 'config.py'
    elif job_type == 2:
        job_name = 'configII.py'
    else:
        print('unknown job type, it should be 0, 1, 2')
        return

    if stderr:
        print("Some errors!")
        print(stderr)
        return

    name = os.path.join('./', name)
    if os.path.isdir(name) and os.listdir(name):
        print("The folder %s is not empty" % name)
        return
    os.makedirs(name)

    if stdout:
        a = stdout.split()
        a=a[0]
        lattice_path = os.path.dirname(a)
        submit = os.path.join(lattice_path, 'pyorbit.submit')
        lattice = os.path.join(lattice_path, job_name)
        shutil.copy2(submit, os.path.join(name, 'pyorbit.submit'))
        shutil.copy2(lattice, os.path.join(name, job_name))
    else:
        print("I cna't find the lattice path! Please make sure csns_lattice is in"
                " your PATH environment!")
        return

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('job_type', type=int, help="id")
    parser.add_argument('name', type=str, help="directory name")
    args = parser.parse_known_args()
    create_newtrack(args[0].job_type, args[0].name)



