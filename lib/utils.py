import os
import sys
import orbit_mpi

from orbit.utils import orbitFinalize

comm = orbit_mpi.mpi_comm.MPI_COMM_WORLD
rank = orbit_mpi.MPI_Comm_rank(comm)

def check_folder(folder):
    '''check if the folder exists or is empty. This is parallel version'''
    if os.path.exists(folder) and os.path.isdir(folder):
        if os.listdir(folder):
            msg = "Warning! The folder %s is not empty, process stop!" % folder
            msg = msg + " Please move out your data in this folder!"
            orbitFinalize(msg)
    else:
        if not os.path.exists(folder): #check again due to parallel calculation
            try:
                os.makedirs(folder)
            except:
                msg = "Thd folder %s already created!" % folder
                print(msg)

def create_folder(folder):
    '''check if the folder exists or is empty. This is parallel version'''
    if os.path.exists(folder) and os.path.isdir(folder):
        if os.listdir(folder):
            msg = "Warning! The folder %s is not empty!" % folder
    else:
        if not os.path.exists(folder): #check again due to parallel calculation
            try:
                os.makedirs(folder)
            except:
                msg = "Thd folder %s already created!" % folder
                print(msg)
