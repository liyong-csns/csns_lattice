from itertools import chain

def read_painting_bump(fileName):
    file_in = open(fileName,'r')
    input_lines = file_in.readlines()
    file_in.close()
    input_lines_split = map(lambda i:input_lines[i].split(), range(len(input_lines)))
    #remove commented lines
    for i in input_lines_split[::-1]:
        if ('!' in ''.join(i)) or ('#' in ''.join(i)):
            input_lines_split.remove(i)
    input_list = list(chain.from_iterable(input_lines_split))
    time = [float(j) for j in input_list[::3]]
    time = tuple(time)
    paintingX = [float(j) for j in input_list[1::3]]
    bumpX = tuple(paintingX)
    paintingY = [float(j) for j in input_list[2::3]]
    bumpY = tuple(paintingY)
    return time,bumpX,bumpY
